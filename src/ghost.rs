use ghost_cell::{GhostCell, GhostToken};
use static_rc::StaticRc;

type Half<'a, T> = StaticRc<GhostCell<'a, Node<'a, T>>, 1, 2>;
type Full<'a, T> = StaticRc<GhostCell<'a, Node<'a, T>>, 2, 2>;

struct Node<'a, T> {
    elem: T,
    prev: Option<Half<'a, T>>,
    next: Option<Half<'a, T>>,
}

impl<'a, T> Node<'a, T> {
    fn new(elem: T) -> Self {
        Self {
            elem,
            prev: None,
            next: None,
        }
    }
}

pub struct LinkedList<'a, T> {
    token: GhostToken<'a>,
    length: usize,
    head_tail: Option<(Half<'a, T>, Half<'a, T>)>,
}

impl<'a, T> LinkedList<'a, T> {
    pub fn new(token: GhostToken<'a>) -> Self {
        Self {
            token,
            length: 0,
            head_tail: None,
        }
    }
}

impl<'a, T> LinkedList<'a, T> {
    pub fn len(&self) -> usize {
        self.length
    }

    pub fn push_back(&mut self, elem: T) {
        let ptr = Full::new(GhostCell::new(Node::new(elem)));
        let (p1, p2) = StaticRc::split::<1, 1>(ptr);
        self.length += 1;
        match &mut self.head_tail {
            Some((_, tail)) => {
                (**tail).borrow_mut(&mut self.token).next = Some(p1);
                let old_tail = std::mem::replace(tail, p2);
                (**tail).borrow_mut(&mut self.token).prev = Some(old_tail);
            }
            None => self.head_tail = Some((p1, p2)),
        }
    }

    pub fn push_front(&mut self, elem: T) {
        let ptr = Full::new(GhostCell::new(Node::new(elem)));
        let (p1, p2) = StaticRc::split::<1, 1>(ptr);
        self.length += 1;
        match &mut self.head_tail {
            Some((head, _)) => {
                (**head).borrow_mut(&mut self.token).prev = Some(p1);
                let old_head = std::mem::replace(head, p2);
                (**head).borrow_mut(&mut self.token).next = Some(old_head);
            }
            None => self.head_tail = Some((p1, p2)),
        }
    }

    pub fn pop_back(&mut self) -> Option<T> {
        let mut result = None;
        self.head_tail.take().map(|(head, p1)| {
            self.length -= 1;
            let new_tail = (*p1).borrow_mut(&mut self.token).prev.take();
            let (p2, head_tail) = match new_tail {
                Some(new_tail) => (
                    (*new_tail).borrow_mut(&mut self.token).next.take().unwrap(),
                    Some((head, new_tail)),
                ),
                None => (head, None),
            };
            self.head_tail = head_tail;
            let ptr = Full::into_inner(Full::join(p1, p2));
            result = Some(ptr.into_inner().elem);
        });
        result
    }

    pub fn pop_front(&mut self) -> Option<T> {
        let mut result = None;
        self.head_tail.take().map(|(p1, tail)| {
            self.length -= 1;
            let new_head = (*p1).borrow_mut(&mut self.token).next.take();
            let (p2, head_tail) = match new_head {
                Some(new_head) => (
                    (*new_head).borrow_mut(&mut self.token).prev.take().unwrap(),
                    Some((new_head, tail)),
                ),
                None => (tail, None),
            };
            self.head_tail = head_tail;
            let ptr = Full::into_inner(Full::join(p1, p2));
            result = Some(ptr.into_inner().elem);
        });
        result
    }
}
