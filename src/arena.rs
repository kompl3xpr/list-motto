use slotmap::{new_key_type, SlotMap};

new_key_type! {
    pub struct NodePtr;
}

pub struct Node<T> {
    elem: T,
    prev: Option<NodePtr>,
    next: Option<NodePtr>,
}

impl<T> Node<T> {
    fn new(elem: T) -> Self {
        Self {
            elem,
            prev: None,
            next: None,
        }
    }
}

pub struct LinkedList<T> {
    arena: SlotMap<NodePtr, Node<T>>,
    length: usize,
    head_tail: Option<(NodePtr, NodePtr)>,
}

impl<T> LinkedList<T> {
    pub fn new() -> Self {
        Self {
            arena: SlotMap::with_key(),
            length: 0,
            head_tail: None,
        }
    }
}

impl<T> LinkedList<T> {
    pub fn push_back(&mut self, elem: T) {
        let ptr = self.arena.insert(Node::new(elem));
        self.length += 1;
        match &mut self.head_tail {
            Some((_, tail)) => {
                self.arena.get_mut(*tail).unwrap().next = Some(ptr);
                self.arena.get_mut(ptr).unwrap().prev = Some(*tail);
                *tail = ptr;
            }
            None => self.head_tail = Some((ptr, ptr)),
        }
    }

    pub fn push_front(&mut self, elem: T) {
        let ptr = self.arena.insert(Node::new(elem));
        self.length += 1;
        match &mut self.head_tail {
            Some((head, _)) => {
                self.arena.get_mut(*head).unwrap().prev = Some(ptr);
                self.arena.get_mut(ptr).unwrap().next = Some(*head);
                *head = ptr;
            }
            None => self.head_tail = Some((ptr, ptr)),
        }
    }

    pub fn pop_back(&mut self) -> Option<T> {
        let mut result = None;
        self.head_tail = self.head_tail.and_then(|(head, tail)| {
            self.length -= 1;
            let tail_node = self.arena.remove(tail).unwrap();
            let new_tail = tail_node.prev;
            result = Some(tail_node.elem);
            new_tail.map(|new_tail| {
                self.arena.get_mut(new_tail).unwrap().next = None;
                (head, new_tail)
            })
        });
        result
    }

    pub fn pop_front(&mut self) -> Option<T> {
        let mut result = None;
        self.head_tail = self.head_tail.and_then(|(head, tail)| {
            self.length -= 1;
            let head_node = self.arena.remove(head).unwrap();
            let new_head = head_node.next;
            result = Some(head_node.elem);
            new_head.map(|new_head| {
                self.arena.get_mut(new_head).unwrap().prev = None;
                (new_head, tail)
            })
        });
        result
    }

    pub fn len(&self) -> usize {
        self.length
    }
}

#[cfg(test)]
mod tests {
    use super::LinkedList;

    #[test]
    fn it_works() {
        let mut list = LinkedList::new();
        for i in 0..10 {
            list.push_back(i);
        }
    }
}
