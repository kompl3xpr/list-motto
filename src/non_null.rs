use std::ptr::NonNull;

type NodePtr<T> = NonNull<Node<T>>;

pub struct Node<T> {
    elem: T,
    prev: Option<NodePtr<T>>,
    next: Option<NodePtr<T>>,
}

impl<T> Node<T> {
    fn new(elem: T) -> Self {
        Self {
            elem,
            prev: None,
            next: None,
        }
    }
}

pub struct LinkedList<T> {
    head_tail: Option<(NodePtr<T>, NodePtr<T>)>,
    length: usize,
}

impl<T> LinkedList<T> {
    pub fn new() -> Self {
        Self {
            head_tail: None,
            length: 0,
        }
    }
}

impl<T> LinkedList<T> {
    pub fn len(&self) -> usize {
        self.length
    }

    pub fn push_back(&mut self, elem: T) {
        let mut ptr = Box::leak(Box::new(Node::new(elem))).into();
        self.length += 1;
        match &mut self.head_tail {
            Some((_, tail)) => unsafe {
                tail.as_mut().next = Some(ptr);
                ptr.as_mut().prev = Some(*tail);
                *tail = ptr;
            },
            None => self.head_tail = Some((ptr, ptr)),
        }
    }

    pub fn push_front(&mut self, elem: T) {
        let mut ptr = Box::leak(Box::new(Node::new(elem))).into();
        self.length += 1;
        match &mut self.head_tail {
            Some((head, _)) => unsafe {
                head.as_mut().prev = Some(ptr);
                ptr.as_mut().next = Some(*head);
                *head = ptr;
            },
            None => self.head_tail = Some((ptr, ptr)),
        }
    }

    pub fn pop_back(&mut self) -> Option<T> {
        let mut result = None;
        self.head_tail = self.head_tail.and_then(|(head, tail)| unsafe {
            self.length -= 1;
            let tail_node = Box::from_raw(tail.as_ptr());
            let new_tail = tail_node.prev;
            result = Some(tail_node.elem);
            new_tail.map(|mut new_tail| {
                new_tail.as_mut().next = None;
                (head, new_tail)
            })
        });
        result
    }

    pub fn pop_front(&mut self) -> Option<T> {
        let mut result = None;
        self.head_tail = self.head_tail.and_then(|(head, tail)| unsafe {
            self.length -= 1;
            let head_node = Box::from_raw(head.as_ptr());
            let new_head = head_node.next;
            result = Some(head_node.elem);
            new_head.map(|mut new_head| {
                new_head.as_mut().prev = None;
                (new_head, tail)
            })
        });
        result
    }
}
