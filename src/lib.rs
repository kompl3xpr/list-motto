pub mod arena;
pub mod ghost;
pub mod non_null;

pub trait DoublyLinkedList {
    type Elem;

    fn len(&self) -> usize;
    fn push_back(&mut self, elem: Self::Elem);
    fn push_front(&mut self, elem: Self::Elem);
    fn pop_back(&mut self) -> Option<Self::Elem>;
    fn pop_front(&mut self) -> Option<Self::Elem>;
}

pub struct IntoIter<T, L: DoublyLinkedList<Elem = T>>(L);

impl<T, L: DoublyLinkedList<Elem = T>> Iterator for IntoIter<T, L> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop_front()
    }
}

macro_rules! impl_list {
    (< $($g: tt),* > for $l: ty where Item = $item: ty) => {
        impl< $($g),* > DoublyLinkedList for $l {
            type Elem = $item;
            fn len(&self) -> usize { self.len() }
            fn push_back(&mut self, elem: Self::Elem) { self.push_back(elem) }
            fn push_front(&mut self, elem: Self::Elem) { self.push_front(elem) }
            fn pop_back(&mut self) -> Option<Self::Elem> { self.pop_back() }
            fn pop_front(&mut self) -> Option<Self::Elem> { self.pop_front() }
        }
        impl< $($g),* > IntoIterator for $l {
            type Item = $item;
            type IntoIter = IntoIter<T, $l>;
            fn into_iter(self) -> Self::IntoIter { IntoIter(self) }
        }
    };
}

impl_list!(<T> for arena::LinkedList<T> where Item = T);
impl_list!(<'a, T> for ghost::LinkedList<'a, T> where Item = T);
impl_list!(<T> for non_null::LinkedList<T> where Item = T);
